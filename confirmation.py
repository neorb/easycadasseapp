"""Methods used to render the confirmation page"""

from os import path
import jinja2
TEMPLATES_DIR = path.join(path.dirname(__file__), "templates")
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(TEMPLATES_DIR))

from domain import AvailabilityLevel


def show(parameters):
    """Generate html for the confirmation page, for the given parameters."""
    # inject details to display about the request
    name = "<b>%s</b>" % parameters["name"]
    level = "<b>%s</b>" % AvailabilityLevel(parameters["state"])
    time_from = "<b>\"nu\"</b>"
    time_to = "<b>\"de eerstvolgende %su\"</b>" % parameters["until"]
    parameters["details"] = "%s<br>%s<br>%svan %s<br>%stot %s" % (
                                            name,
                                            level,
                                            "&#160;"*6,
                                            time_from,
                                            "&#160;"*6,
                                            time_to)
    template = JINJA_ENVIRONMENT.get_template("confirmation.html")
    return template.render(parameters)
