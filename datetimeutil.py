"""Methods to aid with the processing of datetime information, specific for
the easycad app
"""

from datetime import datetime, timedelta
from pytz.gae import pytz

def datetime_in_dutch(datetime_in):
    """Returns a Dutch representation of the given datetime

    For reasons of simplicity (don't kill a fly with a bazooka) it was chosen
    not to go through any localization framework, but manually translate the
    abbreviated names of the week days.

    """
    # Can't easyly change the locale in AppEngine.
    # Not willing to setup a whole translation framework here either.
    trans_dict = {"Mon": "ma", "Tue": "di", "Wed": "wo", "Thu": "do",
                  "Fri": "vr", "Sat": "za", "Sun": "zo",}
    day_en = datetime_in.strftime("%a")
    day_nl = trans_dict[day_en]
    return "%s %s" % (day_nl, datetime_in.strftime("%d/%m %H:%M"))

def is_valid_time(input_str):
    """Checks the given string to see if can extract a valid time from it.

    E.g. "7:00" or "23:30" or "12:01" or even "9" would be considered valid.

    """
    # we allow just a single hour as well:
    if not ":" in input_str:
        return _is_valid_number(input_str, 24)
    else:
        parts = input_str.split(":")
        if len(parts) == 2:
            hour, minute = parts
            return _is_valid_number(hour, 24) and _is_valid_number(minute, 60)
    return False

def _is_valid_number(input_str, maximum):
    """Checks if the given input_str string can represent a positive number,
    smaller than the given maximum.

    """
    try:
        number = int(input_str)
        if number >= 0 and number < maximum:
            return True
    except ValueError:
        return False
    return False

def convert_to_time(input_str):
    """Converts the given string to a datetime.

    Use the is_valid_time method to check for the validity of the string.

    """
    if not ":" in input_str:
        input_str = input_str + ":00"
    return datetime.strptime(input_str, "%H:%M").time()

def get_local_time():
    """Returns the current local time inCET (Brussels)"""
    utc = pytz.utc
    cet = pytz.timezone('CET')
    return utc.localize(datetime.now()).astimezone(cet)

def next_minute():
    """Returns a datetime in CET (Brussels) exactly 1 minute and 5 seconds
    from "now".

    The 1 minute is used to get the next minute, instead of the currently
    minute, which would be in the past if seconds were removed.

    The 5 seconds are added to take into account a certain latency in the
    response of the easycad system.

    """
    now_here = get_local_time()
    # Adding (temporarily) 2 minutes extra to the timedelta, since is seems
    # that the server in Asse is getting very wrong again.
    return now_here + timedelta(minutes=3) + timedelta(seconds=5)

def next_datetime(start_time, end_time):
    """Returns the next datetime after start_time that has end_time as time.
    E.g. if it's now 21:34, then the next 07:00 is tomorrow.

    """
    strt = start_time
    end = end_time
    result = datetime(strt.year, strt.month, strt.day, end.hour, end.minute)
    if start_time.time() < end_time:
        return result
    else:
        return result + timedelta(days=1)
