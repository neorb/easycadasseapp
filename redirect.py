"""Methods used to render the redirect page"""

from os import path
import jinja2
TEMPLATES_DIR = path.join(path.dirname(__file__), "templates")
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(TEMPLATES_DIR))

def show(parameters):
    """Generate html for the redirect page, for the given parameters."""
    template = JINJA_ENVIRONMENT.get_template("redirect.html")
    return template.render(parameters)
