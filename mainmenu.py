"""Methods used to render the mainmenu page"""

from os import path
import jinja2
TEMPLATES_DIR = path.join(path.dirname(__file__), "templates")
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(TEMPLATES_DIR))

def show(user_settings):
    """Generate html for the mainmenu page, for the given parameters."""
    template_values = {}
    if user_settings.remember:
        template_values = {"until": user_settings.until,
                           "username": user_settings.username,
                           "password": user_settings.password,
                           "remember": "checked",}
    else:
        template_values = {"until": "7:00",
                           "username": "",
                           "password": "",
                           "remember": "",}
    template = JINJA_ENVIRONMENT.get_template("mainmenu.html")
    return template.render(template_values)
