"""Methods used to render the history page"""

import webapp2
from os import path
import jinja2
TEMPLATES_DIR = path.join(path.dirname(__file__), "templates")
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(TEMPLATES_DIR))
from domain import HistoryImage

def show():
    """Generate html for the history page, getting the image from the datastore.

    """
    template = JINJA_ENVIRONMENT.get_template("genericredirect.html")
    return template.render({"url": "history",})


class History(webapp2.RequestHandler):
    """Generate html for the history page, getting the image from the datastore.

    """
    def get(self):
        """Http get method/operation on the history page address.

        Returns a very simple history page, showing just one graph, based on
        the template.

        """
        image = HistoryImage.get_by_key_name("Last48h").image
        station_state_image_src = image.encode("base64").strip()

        template = JINJA_ENVIRONMENT.get_template("history.html")
        result = template.render({
                    "station_state_image_src": station_state_image_src,})
        self.response.out.write(result)
