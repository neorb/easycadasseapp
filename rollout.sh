#!/bin/bash

# The old windows way:
##python "c:\Program Files\Google\google_appengine\appcfg.py" update .

# The linux way (appcfg.py should be in the path):
python2 ../google_appengine/appcfg.py update .
