""""Takes a snapshot of the current state of the station.

In practice it performs the get_state_dilbeek workflow and stores the result
in the datastore.
"""

import webapp2
import logging
from pytz.gae import pytz
from datetime import datetime
import time
from domain import UserInfo, StationHistory
import easycadworkflows
import graphlib

class Snapshot(webapp2.RequestHandler):
    """Http request handler that takes a snapshot of a current station state.

    """
    def get(self):
        """Http get method/operation that simply takes a snapshot.

        Returns absolutely nothing.

        """
        time_now = self.local_now()
        timeblock = self.round_to_timeblock(time_now, 15)
        today = time_now.date()
        index = self.round_to_time_index(time_now, 15)
        rabbit_user_name = "delol"
        user_info = UserInfo.get_by_key_name(rabbit_user_name)

        for i in range(10): # We give it 10 trials
            logging.info("Taking snapshot for %s at %s" % (
                                    timeblock.strftime("%Y%m%d %H:%M:%S"),
                                    time_now.strftime("%Y%m%d %H:%M:%S")))
            results = easycadworkflows.get_state_dilbeek(user_info.key().name(),
                                                     user_info.password)
            if "object" in results:
                # Get the station history for "today"
                station_history = StationHistory.all().filter(
                                                    "day_date =", today).get()
                if not station_history:
                    # If it doesn't exist, we create it
                    station_history = StationHistory()
                    station_history.initialize(results["object"].station, today)
                # Now add the current station state to today's record
                station_history.set_station_state(index, results["object"])
                # And store it in the database
                station_history.put()
                logging.info("Snapshot OK")
                time.sleep(2) # give the datastore some time for the put
                graphlib.generate_last_48h_graph()
                logging.info("Generation of graph OK")
                break
            else:
                logging.info("Snapshot failed!")
                logging.info("Waiting 20 seconds.")
                time.sleep(20)

    def local_now(self):
        """Returns the current time in CET."""
        cet = pytz.timezone('CET')
        time_utc = datetime.utcnow()
        time_cet = pytz.utc.localize(time_utc).astimezone(cet)
        return time_cet

    def round_to_timeblock(self, dt_in, blocklength):
        minute = dt_in.minute / 15 * 15
        return datetime(dt_in.year, dt_in.month, dt_in.day,
                        dt_in.hour, minute)

    def round_to_time_index(self, dt_in, blocklength):
        return (dt_in.hour * 60 + dt_in.minute) / blocklength
