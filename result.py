"""Methods used to render the result page"""

from os import path
import jinja2
TEMPLATES_DIR = path.join(path.dirname(__file__), "templates")
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(TEMPLATES_DIR))

def show(results):
    """Generate html for the result page, for the given result parameters."""
    template = JINJA_ENVIRONMENT.get_template("result.html")
    return template.render(results)
