"""Methods used to render images (graphs) and store them in the datastore"""

import StringIO
from datetime import datetime, time, timedelta
from domain import StationHistory, HistoryImage
from google.appengine.ext import db

import matplotlib as mpl
##from matplotlib import patheffects
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg
import matplotlib.dates as mdates
from matplotlib.ticker import MultipleLocator

def generate_last_48h_graph():
    """Generate graph for the history page, showing the last 48 hours."""
    _set_matplotlib_parameters()
    times, state_data = _collect_station_state_data()
    fig = _create_station_state_graph_figure(times, state_data)
    image_str = _create_image(fig)
    _store_image(image_str)

class StationHistoryData():
    pass

def _collect_station_state_data():
    """Gets the data about the station state in the last days from the
    datastore and formats it in such a way that it can be used by matplotlib.

    """
    days_to_fetch = 2
    recs_to_fetch = days_to_fetch + 1

    times = []
    state_data = StationHistoryData()
    state_data.on_intervention = []
    state_data.in_station = []
    state_data.in_VTO_in_station = []
    state_data.on_5_minutes = []
    state_data.on_10_minutes = []
    state_data.on_15_minutes = []
    state_data.on_5_plus_minutes = []
    state_data.on_10_plus_minutes = []
    state_data.on_15_plus_minutes = []
    state_data.in_reserve = []

    # Just get all the records we want and put them in a list:
    station_histories = []
    for rec in (StationHistory.all().order('-day_date').fetch(recs_to_fetch)):
        station_histories.append(rec)

    # Iterate over this list in reverse order:
    station_histories.reverse()
    for stat_his in station_histories:
        day_datetime = datetime.combine(stat_his.day_date, time())
        amount_of_entries = len(stat_his.firechiefs_counts)

        # Temporary code to survive the migration to having in_VTO_in_station:
        if not stat_his.in_VTO_in_station_counts:
            stat_his.in_VTO_in_station_counts = [0]*amount_of_entries

        # Temporary code to survive the migration to having the "plus" states:
        if not stat_his.on_5_plus_minutes_counts:
            stat_his.on_5_plus_minutes_counts = [0]*amount_of_entries
        if not stat_his.on_10_plus_minutes_counts:
            stat_his.on_10_plus_minutes_counts = [0]*amount_of_entries
        if not stat_his.on_15_plus_minutes_counts:
            stat_his.on_15_plus_minutes_counts = [0]*amount_of_entries

        for i in range(amount_of_entries):
            times.append(day_datetime + timedelta(float(i)/amount_of_entries))
            state_data.on_intervention.append(
                                   stat_his.on_intervention_counts[i])
            state_data.in_station.append(stat_his.in_station_counts[i])
            state_data.in_VTO_in_station.append(
                                   stat_his.in_VTO_in_station_counts[i])
            state_data.on_5_minutes.append(stat_his.on_5_minutes_counts[i])
            state_data.on_10_minutes.append(stat_his.on_10_minutes_counts[i])
            state_data.on_15_minutes.append(stat_his.on_15_minutes_counts[i])
            state_data.on_5_plus_minutes.append(stat_his.on_5_plus_minutes_counts[i])
            state_data.on_10_plus_minutes.append(stat_his.on_10_plus_minutes_counts[i])
            state_data.on_15_plus_minutes.append(stat_his.on_15_plus_minutes_counts[i])
            state_data.in_reserve.append(stat_his.in_reserve_counts[i])

    # Determine the amount of empty slots at the end
    # (we don't want to show those)
    last_day_history = station_histories[-1]
    entries_per_day = len(last_day_history.firechiefs_counts)
    amount_empty = 0
    for i in range(entries_per_day - 1, -1, -1):
        if last_day_history.on_intervention_details[i]:
            # found a non empty one
            amount_empty = entries_per_day - i
            break

    # Select the amount of relevant records, covering the requested amount of
    # days
    start = len(times)-amount_empty - (days_to_fetch * entries_per_day) + 1
    end = len(times)-amount_empty + 1
    # Do the necessary chopping:
    times = times[start:end]
    state_data.on_intervention = state_data.on_intervention[start:end]
    state_data.in_station = state_data.in_station[start:end]
    state_data.in_VTO_in_station = state_data.in_VTO_in_station[start:end]
    state_data.on_5_minutes = state_data.on_5_minutes[start:end]
    state_data.on_10_minutes = state_data.on_10_minutes[start:end]
    state_data.on_15_minutes = state_data.on_15_minutes[start:end]
    state_data.on_5_plus_minutes = state_data.on_5_plus_minutes[start:end]
    state_data.on_10_plus_minutes = state_data.on_10_plus_minutes[start:end]
    state_data.on_15_plus_minutes = state_data.on_15_plus_minutes[start:end]
    state_data.in_reserve = state_data.in_reserve[start:end]
    return times, state_data

def _create_station_state_graph_figure(times, state_data):
    factor = len(times)/192.
    fig = Figure(dpi=100, figsize=(factor*10, 6))
    border_left = 0.1 / factor
    border_right = 0.05 / factor
    border_down = 0.2
    border_top = 0.05
    axes = fig.add_axes([border_left,
                         border_down,
                         1 - border_left - border_right,
                         1 - border_down - border_top])
    axes.set_ylabel('Beschikbaarheden')
    ## axes.set_xlabel('Tijd') # Removed xlabel, because it's just silly
    bar_width = 0.01

    up_to_on_intervention = state_data.on_intervention
    up_to_in_station = map(sum, zip(up_to_on_intervention,
                                    state_data.in_station))
    up_to_in_VTO_in_station = map(sum, zip(up_to_in_station,
                                    state_data.in_VTO_in_station))
    up_to_5_minutes = map(sum, zip(up_to_in_VTO_in_station,
                                   state_data.on_5_minutes))
    up_to_5_plus_minutes = map(sum, zip(up_to_5_minutes,
                                   state_data.on_5_plus_minutes))
    up_to_10_minutes = map(sum, zip(up_to_5_plus_minutes,
                                    state_data.on_10_minutes))
    up_to_10_plus_minutes = map(sum, zip(up_to_10_minutes,
                                    state_data.on_10_plus_minutes))
    up_to_15_minutes = map(sum, zip(up_to_10_plus_minutes,
                                    state_data.on_15_minutes))
    up_to_15_plus_minutes = map(sum, zip(up_to_15_minutes,
                                    state_data.on_15_plus_minutes))

    # The actual bars
    axes.bar(times, up_to_15_plus_minutes, width=bar_width,
             facecolor="#804040", edgecolor="#804040")
    axes.bar(times, up_to_15_minutes, width=bar_width,
             facecolor="#804040", edgecolor="#804040")
    axes.bar(times, up_to_10_plus_minutes, width=bar_width,
             facecolor="#FF8000", edgecolor="#FF8000")
    axes.bar(times, up_to_10_minutes, width=bar_width,
             facecolor="#FF8000", edgecolor="#FF8000")
    axes.bar(times, up_to_5_plus_minutes, width=bar_width,
             facecolor="#0080FF", edgecolor="#0080FF")
    axes.bar(times, up_to_5_minutes, width=bar_width,
             facecolor="#0080FF", edgecolor="#0080FF")
    axes.bar(times, up_to_in_VTO_in_station, width=bar_width,
             facecolor="#FF8080", edgecolor="#FF8080")
    axes.bar(times, up_to_in_station, width=bar_width,
             facecolor="Lime", edgecolor="Lime")
    axes.bar(times, up_to_on_intervention, width=bar_width,
             facecolor="Yellow", edgecolor="Yellow")

    # Horizontal grid lines
    axes.axhline(y=6, color="k", linestyle="dashed", alpha=0.5, linewidth=0.5)
    axes.axhline(y=8, color="k", linestyle="dashed", alpha=0.5, linewidth=0.5)

    begin = times[0] + timedelta(minutes=0)
    end = times[-1] + timedelta(minutes=30)
    axes.axis([begin, end, 0, 25])
    fig.autofmt_xdate(rotation=55)
    formatter = mdates.DateFormatter('%d/%m %H')
    def my_strf_time(dt_in, fmt):
        # Add one second to the time, before rounding it to the hour, because
        # we see strange things happening with the roundings from time to time.
        dt = dt_in + timedelta(seconds=1)
        days = ["ma", "di", "wo", "do", "vr", "za", "zo"]
        return "%s %s  %du" % (days[dt.weekday()], dt.day, dt.hour)
    formatter.strftime = my_strf_time
    axes.xaxis.set_major_formatter(formatter)
    axes.xaxis.set_major_locator(MultipleLocator(base=1./12))

    return fig

def _set_matplotlib_parameters():
    """Sets some parameters for matplotlib."""
    mpl.rcParams['lines.linewidth'] = 6
    mpl.rcParams['font.family'] = ['Humor Sans', 'Comic Sans MS']
    mpl.rcParams['font.size'] = 14.0
    # Following 2 params commented out because they simply don't exist yet
    # in version 1.2.0 of Matplotlib (used by GAE)
    # We'll have to wait for GAE to support version 1.3.0
    ##mpl.rcParams['path.sketch'] = (1, 100, 2)
    ##mpl.rcParams['path.effects'] = [patheffects.withStroke(
    ##                                      linewidth=4, foreground="w")]
    mpl.rcParams['axes.linewidth'] = 1.5
    mpl.rcParams['lines.linewidth'] = 2.0
    mpl.rcParams['figure.facecolor'] = 'white'
    mpl.rcParams['grid.linewidth'] = 0.0
    mpl.rcParams['axes.unicode_minus'] = False
    mpl.rcParams['axes.color_cycle'] = ['b', 'r', 'c', 'm']
    mpl.rcParams['xtick.major.size'] = 8
    mpl.rcParams['xtick.major.width'] = 3
    mpl.rcParams['ytick.major.size'] = 8
    mpl.rcParams['ytick.major.width'] = 3

def _create_image(figure):
    """Takes a Matplotlig Figure and stores it in the datastore."""
    FigureCanvasAgg(figure)
    image_str = StringIO.StringIO()
    figure.savefig(image_str)
    return image_str

def _store_image(image_str):
    history_image = HistoryImage(key_name="Last48h")
    history_image.image = db.Blob(image_str.getvalue())
    history_image.put()
