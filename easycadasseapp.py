# coding=utf-8
"""Contains the main easycadasse app.

This "app" consists of a few webpages that can be used to speed up and
simplify the use of an existing 3rd party webpage on https://easycad.zvbw.be/.
Instead of having to log in on this page and navigate through different pages,
filling in values and pushing buttons, this app will do all that for you with
just one or two clicks.
It will use cookies to remember your username, password and last entered
values, so that you don't have to fill in anything and just have to press one
button + confirmation to change your availability state, or just have to press
one button to see the state of the station's people.
"""

import webapp2

import mainmenu
import result
import redirect
import confirmation
import graph
import history

import easycadworkflows
from logs import Logs
from stats import Stats
from history import History
from snapshot import Snapshot
import userutil
from loglib import log_action


class MainMenu(webapp2.RequestHandler):
    """Class for the main entry point"""

    def get(self):
        """Main http get method/operation on the app's entry point address
        This does nothing more than just showing the main menu (which is the
        first page). It will also first handle the necessary stuff to find out
        who is the user and fill in the right default values in the main
        menu's text fields.

        """
        user_settings = userutil.handle_user_settings(self)
        self.response.out.write(mainmenu.show(user_settings))


class EasyCadAsseApp(webapp2.RequestHandler):
    """Class that handles all the post requests in the entire app"""

    def get(self):
        """Simple http get method/operation on the EasyCadAsseApp page.
        Normally this will only be called when you do a refresh of the page
        after it was loaded via a post operation.

        """
        user_settings = userutil.handle_user_settings(self)
        self.response.out.write(mainmenu.show(user_settings))

    def post(self):
        """Post operation handler that is at the hearth of the application
        and handles all possible user actions.

        """
        if "chb_remember" in self.request.params:
            userutil.save_user_settings(self)
        if username_is_empty(self.request):
            self._warn_about_missing_username()
        elif password_is_empty(self.request):
            self._warn_about_missing_password()
        elif "btn_state_dilbeek" in self.request.params:
            self._request_get_state_dilbeek()
        elif "btn_unavailable" in self.request.params:
            self._confirm_change_state("unavailable")
        elif "btn_in_station" in self.request.params:
            self._confirm_change_state("in_station")
        elif "btn_in_VTO_in_station" in self.request.params:
            self._confirm_change_state("in_VTO_in_station")
        elif "btn_on_5" in self.request.params:
            self._confirm_change_state("on_5")
        elif "btn_on_10" in self.request.params:
            self._confirm_change_state("on_10")
        elif "btn_on_15" in self.request.params:
            self._confirm_change_state("on_15")
        elif "btn_on_5_plus" in self.request.params:
            self._confirm_change_state("on_5_plus")
        elif "btn_on_10_plus" in self.request.params:
            self._confirm_change_state("on_10_plus")
        elif "btn_on_15_plus" in self.request.params:
            self._confirm_change_state("on_15_plus")
        elif "btn_ambulance" in self.request.params:
            self._confirm_change_state("ambulance")
        elif "btn_reserve" in self.request.params:
            self._confirm_change_state("reserve")
        elif "btn_test_password" in self.request.params:
            self._request_test_password()
        elif "btn_forget_password" in self.request.params:
            self._forget_password()
        elif "btn_current_state" in self.request.params:
            self._request_get_current_state()
        elif "btn_graph" in self.request.params:
            self._show_graph()
        elif "btn_history" in self.request.params:
            self._show_history()
        elif "btn_to_main_menu" in self.request.params:
            self._to_main_menu()
        elif "btn_confirm_state_change" in self.request.params:
            self._request_change_state()
        elif "redirect_function" in self.request.params:
            redirect_function = self.request.get("redirect_function")
            if redirect_function == "get_state_dilbeek":
                self._get_state_dilbeek()
            elif redirect_function == "change_state":
                self._change_state()
            elif redirect_function == "test_password":
                self._test_password()
            elif redirect_function == "get_current_state":
                self._get_current_state()

    def _warn_about_missing_username(self):
        """Returns a results page stating that the user should fill in a
        username.

        """
        results = {"result_class": "resultNOK", "details": ""}
        results["message"] = "Best toch wel een gebruikersnaam invullen!"
        self.response.out.write(result.show(results))

    def _warn_about_missing_password(self):
        """Returns a results page stating that the user should fill in a
        username.

        """
        results = {"result_class": "resultNOK", "details": ""}
        results["message"] = "Best toch wel een paswoord invullen!"
        self.response.out.write(result.show(results))

    def _request_get_state_dilbeek(self):
        """Shows the redirect page with a message to be patient, while a
        post request is sent to get the current state of the people available
        for the fire station in Dilbeek.

        """
        parameters = {"redirect_function": "get_state_dilbeek"}
        parameters["username"] = self.request.get("txt_username")
        parameters["password"] = self.request.get("txt_password")
        self.response.out.write(redirect.show(parameters))

    def _get_state_dilbeek(self):
        """Gets the state of the fire station in Dilbeek and shows it in a
        result page.

        """
        username = self.request.get("txt_username")
        password = self.request.get("txt_password")
        results = easycadworkflows.get_state_dilbeek(username, password)
        log_action(username, "get_state_dilbeek", results["message"])
        self.response.out.write(result.show(results))

    def _confirm_change_state(self, state):
        """Displays a confirmation page to change the availablility state of
        the current user.

        The confirmation page also gives a summary of the requested state
        and start and end time.

        """
        parameters = {}
        parameters["username"] = self.request.get("txt_username")
        parameters["password"] = self.request.get("txt_password")
        parameters["name"] = userutil.get_real_name(parameters["username"])
        parameters["until"] = self.request.get("txt_until")
        parameters["state"] = state
        self.response.out.write(confirmation.show(parameters))

    def _request_change_state(self):
        """Shows the redirect page with a message to be patient, while a
        post request is sent to change the availability state of the user.

        """
        parameters = {"redirect_function": "change_state"}
        parameters["username"] = self.request.get("txt_username")
        parameters["password"] = self.request.get("txt_password")
        parameters["until"] = self.request.get("txt_until")
        parameters["state"] = self.request.get("state")
        self.response.out.write(redirect.show(parameters))

    def _change_state(self):
        """Tries to change the availability state of the user and shows the
        feedback (ok, failed,...) from the Easycadasse web page.

        """
        username = self.request.get("txt_username")
        password = self.request.get("txt_password")
        until = self.request.get("txt_until")
        state = self.request.get("state")
        results = easycadworkflows.change_state(username, password,
                                                until, state)
        log_action(username, "change_state", results["details"])
        self.response.out.write(result.show(results))

    def _request_test_password(self):
        """Shows the redirect page with a message to be patient, while a
        post request is sent to log in to the system with the given username
        and password.

        """
        parameters = {"redirect_function": "test_password"}
        parameters["username"] = self.request.get("txt_username")
        parameters["password"] = self.request.get("txt_password")
        self.response.out.write(redirect.show(parameters))

    def _test_password(self):
        """Tries to log in to the system with the given username and password
        and shows the feedback (ok, failed) from the Easycadasse web page.

        """
        username = self.request.get("txt_username")
        password = self.request.get("txt_password")
        results = easycadworkflows.test_password(username, password)
        log_action(username, "test_password", results["details"])
        self.response.out.write(result.show(results))

    def _forget_password(self):
        """Deletes the username, password, until-time and remember flag from
        the record that is stored for the user corresponding to the current
        cookie. After a refresh all the personal data will be gone.

        """
        user_settings = userutil.delete_user_settings(self)
        self.response.out.write(mainmenu.show(user_settings))

    def _request_get_current_state(self):
        """Shows the redirect page with a message to be patient, while a
        post request is sent to get the current availability state of the user.

        """
        parameters = {"redirect_function": "get_current_state"}
        parameters["username"] = self.request.get("txt_username")
        parameters["password"] = self.request.get("txt_password")
        self.response.out.write(redirect.show(parameters))

    def _get_current_state(self):
        """Gets the current availability state of the user and shows it in a
        result page.

        """
        username = self.request.get("txt_username")
        password = self.request.get("txt_password")
        results = easycadworkflows.get_current_state(username, password)
        log_action(username, "get_current_state", results["message"])
        self.response.out.write(result.show(results))

    def _show_graph(self):
        """Shows a page with a graph that represents the site usage."""
        self.response.out.write(graph.show())
        pass

    def _show_history(self):
        """Shows a page with a graph that shows the evolution of the amount of
        people that were available in the last 48 hours.

        """
        self.response.out.write(history.show())
        pass

    def _to_main_menu(self):
        """Shows the main menu page (the first page). It will also first
        handle the necessary stuff to find out who is the user and fill in the
        right default values in the main menu's text fields.

        """
        user_settings = userutil.handle_user_settings(self)
        self.response.out.write(mainmenu.show(user_settings))


# helper methods

def username_is_empty(request):
    """Returns True if for a given request the username is empty"""
    if "txt_username" in request.params:
        if request.get("txt_username") == "":
            return True

def password_is_empty(request):
    """Returns True if for a given request the password is empty"""
    if "txt_password" in request.params:
        if request.get("txt_password") == "":
            return True


# webapp2 framework entry point

app = webapp2.WSGIApplication([("/", MainMenu),
                               ("/logs", Logs),
                               ("/stats", Stats),
                               ("/history", History),
                               ("/snapshot", Snapshot),
                               ("/easycadasseapp", EasyCadAsseApp),],
                               debug=True)
