""""Contains all that is necessary to show the "logs" webpage.

Consists of
- The class Logs: The webpage itself.
- The class UserInfoManager: Helper class to manage all UserInfo objects.
- The class UserActionManager: Helper class to manage all UserAction objects.

"""

import webapp2
from pytz.gae import pytz
from domain import UserAction, UserInfo, UserActionCount

class Logs(webapp2.RequestHandler):
    """Very basic webpage that generates some recent logging information,
    showing what the users have been up to.

    """
    def get(self):
        """Http get method/operation on the logs page address.

        Returns a very simple logs page, not even as html, but just as simple
        text.

        """
        user_info_manager = UserInfoManager()
        user_action_manager = UserActionManager()
        lines = []
        # get user actions
        for user_action in user_action_manager.user_actions:
            cet = pytz.timezone('CET')
            time_utc = user_action.creation_datetime
            time_cet = pytz.utc.localize(time_utc).astimezone(cet)
            time = time_cet.strftime("%Y%m%d %H:%M:%S")
            username = user_action.username
            real_name = user_info_manager.try_get_real_name(username)
            action = user_action.action
            result = user_action.result.replace("<br>", " ")
            if len(result) > 150:
                result = result[0:150] + "..."
            lines.append("%s - %s - %s - %s" % (time, real_name,
                                                action, result))
        log_lines = "<br>".join(lines)
        self.response.out.write(log_lines)


class UserInfoManager():
    """Class that manages all UserInfo objects and their data access."""

    def __init__(self):
        self.real_name_dict = self._get_real_name_dict()

    def _get_real_name_dict(self):
        """Preloads all the existing UserInfo objects from the datastore into
        a dictionary. This is usually done when initializing the manager.

        """
        real_name_dict = {}
        for user_info in UserInfo.all():
            real_name_dict[user_info.key().name().upper()] = user_info.real_name
        return real_name_dict

    def try_get_real_name(self, username):
        """Checks the existing UserInfo objects to see if there is one with the
        given username and returns the corresponding real name of that person.
        If the username is not found, it returns the username between "<>".

        """
        if username.upper() in self.real_name_dict:
            return self.real_name_dict[username.upper()]
        else:
            return "&lt;%s&gt;" % username


class UserActionManager():
    """Class that manages all UserAction objects and their data access."""

    def __init__(self):
        self.user_actions = self._get_user_actions()

    def _get_user_actions(self):
        """Preloads the 100 most recent UserAction objects from the datastore.
        This is usually done when initializing the manager.

        """
        return UserAction.all().order("-creation_datetime").fetch(1000)
