"""Contains helper methods to log the users actions."""

from domain import UserAction, UserActionCount
import datetimeutil

def log_action(username, action, result):
    """Logs a UserAction in the datastore and augments the day counter for the
    daily amount of user actions.

    """
    # Add the user action
    UserAction(username, action, result).put()
    # Add 1 to the counter for today
    # No sharded counter needed here for the moment ;-)
    today_key = datetimeutil.get_local_time().strftime("%Y%m%d")
    counter = UserActionCount.get_by_key_name(today_key)
    if not counter:
        counter = UserActionCount(key_name=today_key)
    counter.count += 1;
    counter.put()
