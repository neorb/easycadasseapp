"""Contains domain objects (entities,...) used by the easycad app."""

from google.appengine.ext import db
from datetime import datetime

class UserSettings(db.Model):
    """Settings for a user, for one cookie-based session"""
    until = db.StringProperty(default="7:00", indexed=False)
    username = db.StringProperty(default="", indexed=False)
    password = db.StringProperty(default="", indexed=False)
    remember = db.BooleanProperty(default=True, indexed=False)
    creation_datetime = db.DateTimeProperty(auto_now_add=True, indexed=False)
    update_datetime = db.DateTimeProperty(auto_now=True, indexed=False)

    def __str__(self):
        return "Username: %s - %s - Password: %s" % (
                                                    self.username,
                                                    self.creation_datetime,
                                                    self.password)


class UserInfo(db.Model):
    """Info on a unique user, gathered across all sessions"""
    #key_name = username
    real_name = db.StringProperty(default="", indexed=True)
    password = db.StringProperty(default="", indexed=False)
    creation_datetime = db.DateTimeProperty(auto_now_add=True, indexed=True)
    update_datetime = db.DateTimeProperty(auto_now=True, indexed=True)

    def __str__(self):
        return "%s (%s:%s) - C:%s U%s" % (
                            self.key_name, self.real_name, self.password,
                            self.creation_datetime.strftime("%Y%m%d %H:%M"),
                            self.update_datetime.strftime("%Y%m%d %H:%M"),)


class UserAction(db.Model):
    """Logging info about a user performing an action"""
    username = db.StringProperty(default="", indexed=True)
    action = db.StringProperty(default="", indexed=True)
    result = db.TextProperty(default="", indexed=False)
    creation_datetime = db.DateTimeProperty(auto_now_add=True, indexed=True)

    def __init__(self,
                 username_arg=None,
                 action_arg=None,
                 result_arg=None,
                 **kwargs):
        if username_arg:
            db.Model.__init__(self,
                              username=username_arg,
                              action=action_arg,
                              result=result_arg)
        else:
            db.Model.__init__(self, **kwargs)

    def __str__(self):
        return "%s - %s - %s - %s" % (
                            self.username,
                            self.creation_datetime.strftime("%Y%m%d %H:%M"),
                            self.action, self.result,)


class UserActionCount(db.Model):
    """Logging info about the amount of user actions per day"""
    # Key is string representation of the date in the form %Y%m%d
    count = db.IntegerProperty(default=0, indexed=False)

    def __str__(self):
        count_date = datetime.strptime(self.key().name(), "%Y%m%d").date()
        return "%s - %s" % (count_date, self.count)


class StationState():
    """Info about the availability of all personel in a certain station at a
    certain time

    """
    def __str__(self):
        lines = []
        lines.append("Grad&#233;s: %i" % self.firechiefs_count)
        lines.append("Brandweermannen: %i" % self.firefighters_count)
        for label, personel in [("Op interventie", self.on_intervention),
                                ("In kazerne", self.in_station),
                                ("In VTO in kazerne", self.in_VTO_in_station),
                                ("Op 5 minuten", self.on_5_minutes),
                                ("Op 10 minuten", self.on_10_minutes),
                                ("Op 15 minuten", self.on_15_minutes),
                                ("Op 5+ minuten", self.on_5_plus_minutes),
                                ("Op 10+ minuten", self.on_10_plus_minutes),
                                ("Op 15+ minuten", self.on_15_plus_minutes),
                                ("Reserve", self.in_reserve),]:
            if len(personel) > 0:
                lines.append("<br>%s:" % label)
                for name in personel:
                    lines.append("&#160;&#160;- %s" % name)
        return "<br>".join(lines)

    def serialize(self):
        return "%s\t%i\t%i\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" % (
                                                       self.timeblock,
                                                       self.firechiefs_count,
                                                       self.firefighters_count,
                                                       self.on_intervention,
                                                       self.in_station,
                                                       self.in_VTO_in_station,
                                                       self.on_5_minutes,
                                                       self.on_10_minutes,
                                                       self.on_15_minutes,
                                                       self.on_5_plus_minutes,
                                                       self.on_10_plus_minutes,
                                                       self.on_15_plus_minutes,
                                                       self.in_reserve,)

# Internal note:
# StationHistory will relace the old StationState records in the datastore.
# StationState will still exist as a usefull class, but will not be stored in
# the datastore anymore.
# Instead of adding one record every 15 minutes, we will keep one big record per
# day. Since it's just more convenient to work with bigger chunks of data.
# Definitely because now it takes 200 (trivial) queries each time we want a
# simple overview of the last two days. Doing statistics gets really expensive
# this way, while there is absolutely no need to store all this stuff as
# separate records.

class StationHistory(db.Model):
    """Info about the availability of all personel in a certain station for an
    entire day

    We keep lists of the amount of people in a certain state, with one count
    for each of the 96 15 minutes slots in a day.
    The details about exactly which people were in a certain state at a
    certain moment are stored as lists of 96 strings, which each string a
    simple python string representation of the list of people.

    """
    station = db.StringProperty(default="", indexed=True)
    timestamp = db.DateTimeProperty(auto_now=True, indexed=False)
    day_date = db.DateProperty(indexed=True)
    firechiefs_counts = db.ListProperty(int, indexed=False)
    firefighters_counts = db.ListProperty(int, indexed=False)
    on_intervention_counts = db.ListProperty(int, indexed=False)
    on_intervention_details = db.StringListProperty(indexed=False)
    in_station_counts = db.ListProperty(int, indexed=False)
    in_station_details = db.StringListProperty(indexed=False)
    in_VTO_in_station_counts = db.ListProperty(int, indexed=False)
    in_VTO_in_station_details = db.StringListProperty(indexed=False)
    on_5_minutes_counts = db.ListProperty(int, indexed=False)
    on_5_minutes_details = db.StringListProperty(indexed=False)
    on_10_minutes_counts = db.ListProperty(int, indexed=False)
    on_10_minutes_details = db.StringListProperty(indexed=False)
    on_15_minutes_counts = db.ListProperty(int, indexed=False)
    on_15_minutes_details = db.StringListProperty(indexed=False)
    on_5_plus_minutes_counts = db.ListProperty(int, indexed=False)
    on_5_plus_minutes_details = db.StringListProperty(indexed=False)
    on_10_plus_minutes_counts = db.ListProperty(int, indexed=False)
    on_10_plus_minutes_details = db.StringListProperty(indexed=False)
    on_15_plus_minutes_counts = db.ListProperty(int, indexed=False)
    on_15_plus_minutes_details = db.StringListProperty(indexed=False)
    in_reserve_counts = db.ListProperty(int, indexed=False)
    in_reserve_details = db.StringListProperty(indexed=False)

    def initialize(self, station, day_date):
        """Initialize record for all the timeslots in a day.

        This is not intended as a constructor to be used each time an object
        is materialized. It's intended to be used when a record is created for
        the very first time. We create the necessary lists with their intended
        final size, to make certain that even if the snapshot fails, we still
        have an entry. E.g., if we would just append a value to our lists for
        each snapshot, we wouldn't know it if one or two snapshots failed and
        we would end up with a day that only has 94 or 95 timeslots instead of
        96.

        """
        self.station = station
        self.day_date = day_date
        self.firechiefs_counts = [0]*4*24
        self.firefighters_counts = [0]*4*24
        self.on_intervention_counts = [0]*4*24
        self.on_intervention_details = [""]*4*24
        self.in_station_counts = [0]*4*24
        self.in_station_details = [""]*4*24
        self.in_VTO_in_station_counts = [0]*4*24
        self.in_VTO_in_station_details = [""]*4*24
        self.on_5_minutes_counts = [0]*4*24
        self.on_5_minutes_details = [""]*4*24
        self.on_10_minutes_counts = [0]*4*24
        self.on_10_minutes_details = [""]*4*24
        self.on_15_minutes_counts = [0]*4*24
        self.on_15_minutes_details = [""]*4*24
        self.on_5_plus_minutes_counts = [0]*4*24
        self.on_5_plus_minutes_details = [""]*4*24
        self.on_10_plus_minutes_counts = [0]*4*24
        self.on_10_plus_minutes_details = [""]*4*24
        self.on_15_plus_minutes_counts = [0]*4*24
        self.on_15_plus_minutes_details = [""]*4*24
        self.in_reserve_counts = [0]*4*24
        self.in_reserve_details = [""]*4*24

    def set_station_state(self, index, station_state):
        self.firechiefs_counts[index] = station_state.firechiefs_count
        self.firefighters_counts[index] = station_state.firefighters_count
        self.on_intervention_counts[index] = len(station_state.on_intervention)
        self.on_intervention_details[index] = str(station_state.on_intervention)
        self.in_station_counts[index] = len(station_state.in_station)
        self.in_station_details[index] = str(station_state.in_station)
        if not self.in_VTO_in_station_counts:
            self.in_VTO_in_station_counts = [0]*4*24
        if not self.in_VTO_in_station_details:
            self.in_VTO_in_station_details = [""]*4*24
        self.in_VTO_in_station_counts[index] = len(station_state.in_VTO_in_station)
        self.in_VTO_in_station_details[index] = str(station_state.in_VTO_in_station)
        self.on_5_minutes_counts[index] = len(station_state.on_5_minutes)
        self.on_5_minutes_details[index] = str(station_state.on_5_minutes)
        self.on_10_minutes_counts[index] = len(station_state.on_10_minutes)
        self.on_10_minutes_details[index] = str(station_state.on_10_minutes)
        self.on_15_minutes_counts[index] = len(station_state.on_15_minutes)
        self.on_15_minutes_details[index] = str(station_state.on_15_minutes)
        if not self.on_5_plus_minutes_counts:
            self.on_5_plus_minutes_counts = [0]*4*24
        if not self.on_5_plus_minutes_details:
            self.on_5_plus_minutes_details = [""]*4*24
        self.on_5_plus_minutes_counts[index] = len(station_state.on_5_plus_minutes)
        self.on_5_plus_minutes_details[index] = str(station_state.on_5_plus_minutes)
        if not self.on_10_plus_minutes_counts:
            self.on_10_plus_minutes_counts = [0]*4*24
        if not self.on_10_plus_minutes_details:
            self.on_10_plus_minutes_details = [""]*4*24
        self.on_10_plus_minutes_counts[index] = len(station_state.on_10_plus_minutes)
        self.on_10_plus_minutes_details[index] = str(station_state.on_10_plus_minutes)
        if not self.on_15_plus_minutes_counts:
            self.on_15_plus_minutes_counts = [0]*4*24
        if not self.on_15_plus_minutes_details:
            self.on_15_plus_minutes_details = [""]*4*24
        self.on_15_plus_minutes_counts[index] = len(station_state.on_15_plus_minutes)
        self.on_15_plus_minutes_details[index] = str(station_state.on_15_plus_minutes)
        self.in_reserve_counts[index] = len(station_state.in_reserve)
        self.in_reserve_details[index] = str(station_state.in_reserve)
        # To retrieve the names for a certain timeslot again, we can turn
        # the string of people back into a list, using the literal_eval function
        # (Do a "from ast import literal_eval" first)

    def serialize(self):
        return "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" % (
                                                       self.station,
                                                       self.day_date,
                                                       self.on_intervention_details,
                                                       self.in_station_details,
                                                       self.in_VTO_in_station_details,
                                                       self.on_5_minutes_details,
                                                       self.on_10_minutes_details,
                                                       self.on_15_minutes_details,
                                                       self.on_5_plus_minutes_details,
                                                       self.on_10_plus_minutes_details,
                                                       self.on_15_plus_minutes_details,
                                                       self.in_reserve_details,)

class HistoryImage(db.Model):
    """Graph showing the availability of all personel in a certain station for
    a certain period.

    Stored as an image in png format.

    """
    image = db.BlobProperty()


class AvailabilityLevel():
    """Level of availability in the easycad system."""

    def __init__(self, state):
        """Initialize an AvailablityLevel object for the given state.

        The given state is an internaly predefined string.
        The object will contain the code that is used by the EasyCad system
        for that level of availability.
        The description is a nice Dutch string that represents the state and
        can be used to inject in messages for the end user.
        """
        if state == "unavailable":
            self.code = "11"
            self.description = "onbeschikbaar"
        elif state == "in_station":
            self.code = "5"
            self.description = "beschikbaar in de kazerne"
        elif state == "in_VTO_in_station":
            self.code = "16"
            self.description = "in VTO in kazerne"
        elif state == "on_5":
            self.code = "6"
            self.description = "beschikbaar \"op 5 minuten\""
        elif state == "on_10":
            self.code = "7"
            self.description = "beschikbaar \"op 10 minuten\""
        elif state == "on_15":
            self.code = "8"
            self.description = "beschikbaar \"op 15 minuten\""
        elif state == "on_5_plus":
            self.code = "2"
            self.description = "beschikbaar \"op 5+ minuten\""
        elif state == "on_10_plus":
            self.code = "12"
            self.description = "beschikbaar \"op 10+ minuten\""
        elif state == "on_15_plus":
            self.code = "13"
            self.description = "beschikbaar \"op 15+ minuten\""
        elif state == "ambulance":
            self.code = "4"
            self.description = "van ziekenwagen"
        elif state == "reserve":
            self.code = "9"
            self.description = "beschikbaar als \"reserve\""
        else:
            self.code = "-1"
            self.description = None

    def __str__(self):
        return self.description
