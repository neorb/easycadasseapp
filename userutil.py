"""Methods to aid with the processing of user settings, specific for
the easycad app
"""

import uuid
from datetime import datetime, timedelta
from domain import UserSettings, UserInfo

def handle_user_settings(handler):
    """Reads the cookie to find out who the user is, retreives the necessary
    info from the datastore and returns the user settings.
    If it's a new user, a new record is created.

    """
    user_id = handler.request.cookies.get("UserId")
    if not user_id:
        user_id = str(uuid.uuid4())
        expiration = datetime.now() + timedelta(days=1000)
        handler.response.set_cookie("UserId", user_id, expires=expiration)
    return UserSettings.get_or_insert(key_name=user_id)

def save_user_settings(handler):
    """Retreives the user settings from the datastore and updates them with the
    info that was entered by the user in the main menu form.

    """
    user_settings = handle_user_settings(handler)
    user_settings.remember = (handler.request.get("chb_remember") == "on")
    if user_settings.remember:
        user_settings.until = handler.request.get("txt_until")
        user_settings.username = handler.request.get("txt_username")
        user_settings.password = handler.request.get("txt_password")
    else:
        _reset_user_settings(user_settings)
    user_settings.put()

def delete_user_settings(handler):
    """Retreives the user settings from the datastore and updates them with
    blanco values.

    """
    user_settings = handle_user_settings(handler)
    _reset_user_settings(user_settings)
    user_settings.put()
    return user_settings

def _reset_user_settings(user_settings):
    """Updates the given user settings with blanco values."""
    user_settings.until = "7:00"
    user_settings.username = ""
    user_settings.password = ""
    user_settings.remember = False

def get_real_name(username):
    """Tries to get real name, given user name
    If not found, returns the user name.

    """
    user_info = UserInfo.get_by_key_name(username)
    if user_info:
        return user_info.real_name
    else:
        return username
