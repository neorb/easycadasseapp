"""Contains methods that handle entire workflows, meaning the automated
subsequent browsing through multiple webpages in order to perform a certain
action or obtain a certain result."""

from easycadlib import EasyCadServer
from domain import UserInfo
from domain import AvailabilityLevel
from datetimeutil import datetime_in_dutch, is_valid_time, convert_to_time
from datetimeutil import next_minute, next_datetime

def get_state_dilbeek(username, password):
    """Performs the necessary browsing steps to obtain the current personel
    state of the fire station of Dilbeek

    """
    # The station "Dilbeek" is not a parameter, explicitely, by design.
    results = {}
    server = EasyCadServer(username, password)
    if not server.start_page:
        results["result_class"] = "resultNOK"
        results["message"] = "Action failed!"
        results["details"] = ("Er schijnt een probleem te zijn met EasyCad"
                             ". Waarschijnlijk antwoordt EasyCad tijdelij"
                             "k te traag. Gelieve opnieuw te proberen.")
        return results
    if server.start_page == "WrongCredentials":
        results["result_class"] = "resultNOK"
        results["message"] = "Login failed!"
        results["details"] = "Controleer uw gebruikersnaam en paswoord."
        return results
    # We first have to browse to the "Personeelstatus" page:
    if not server.get_personeelstatus("DIL"):
        results["result_class"] = "resultNOK"
        results["message"] = "Sorry!"
        results["details"] = ("U heeft niet het juiste profiel om de "
                             "status van Dilbeek te bekijken.")
        return results
    results["result_class"] = "result"
    server.current_personel_state.station = "Dilbeek"
    results["message"] = str(server.current_personel_state)
    results["details"] = ""
    results["object"] = server.current_personel_state
    return results

def change_state(username, password, until, state):
    """Performs the necessary browsing steps to change the availability state
    of the user from now until the given end "until" time.

    """
    level = AvailabilityLevel(state)
    results = {}
    if not is_valid_time(until):
        results["result_class"] = "resultNOK"
        results["message"] = "Fout!"
        results["details"] = ("Controleer het veld \"...tot\". De ingevulde"
                             " waarde moet van de vorm HH:MM zijn, met HH "
                             "de uren en MM de minuten. Bijvoorbeeld: \"21"
                             ":30\" of \"9:45\". Nu staat er \"%s\"." %
                             until)
        return results
    server = EasyCadServer(username, password)
    if not server.start_page:
        results["result_class"] = "resultNOK"
        results["message"] = "Action failed!"
        results["details"] = ("Er schijnt een probleem te zijn met EasyCad"
                             ". Waarschijnlijk antwoordt EasyCad tijdelij"
                             "k te traag. Gelieve opnieuw te proberen.")
        return results
    if server.start_page == "WrongCredentials":
        results["result_class"] = "resultNOK"
        results["message"] = "Login failed!"
        results["details"] = "Controleer uw gebruikersnaam en paswoord."
        return results
    # For normal users, the startpage after login is the webchart.
    # If not the case, we have to do an extra step and browse to it:
    if not server.start_page == "WebChart":
        if not server.get_beschikbaarheid():
            results["result_class"] = "resultNOK"
            results["message"] = "Failed to load page!"
            results["details"] = ("Applicatie is ingelogd, maar kan de  "
                                 "pagina \"Beschikbaarheid\" niet laden.")
            return results
    # We're on the "Beschikbaarheid" page now
    set_user_info(username, password, server.user_real_name)
    if not server.get_beschikbaarheid_ingeven():
        results["result_class"] = "resultNOK"
        results["message"] = "Failed to load page!"
        results["details"] = ("Applicatie is ingelogd, maar kan de pagina "
                             "om de beschikbaarheden in te geven niet "
                             "laden.")
        return results
    start = next_minute()
    end = next_datetime(start, convert_to_time(until))
    if not server.set_beschikbaarheid(level.code, start, end):
        results["result_class"] = "resultNOK"
        results["message"] = "Failed to submit data!"
        results["details"] = ("Applicatie is ingelogd, maar de ingegeven "
                             "uren worden niet aanvaard.")
        return results
    results["result_class"] = "resultOK"
    results["message"] = "In orde!"
    results["details"] = "U bent %s<br>%svan %s<br>%stot %s." % (
                                        level,
                                        "&#160;"*6,
                                        datetime_in_dutch(start),
                                        "&#160;"*6,
                                        datetime_in_dutch(end))
    return results

def test_password(username, password):
    """Performs the necessary browsing steps to test if it's possible to log in
    with the given username and password.

    """
    results = {}
    server = EasyCadServer(username, password)
    if not server.start_page:
        results["result_class"] = "resultNOK"
        results["message"] = "Action failed!"
        results["details"] = ("Er schijnt een probleem te zijn met EasyCad"
                             ". Waarschijnlijk antwoordt EasyCad tijdelij"
                             "k te traag. Gelieve opnieuw te proberen.")
        return results
    if server.start_page == "WrongCredentials":
        results["result_class"] = "resultNOK"
        results["message"] = "Login failed!"
        results["details"] = "Controleer uw gebruikersnaam en paswoord."
        return results
    results["result_class"] = "resultOK"
    results["message"] = "Login succeeded!"
    results["details"] = "Gebruikersnaam en paswoord zijn correct."
    return results

def get_current_state(username, password):
    """Performs the necessary browsing steps to get the current availability
    state of the user.

    """
    results = {}
    server = EasyCadServer(username, password)
    if not server.start_page:
        results["result_class"] = "resultNOK"
        results["message"] = "Action failed!"
        results["details"] = ("Er schijnt een probleem te zijn met EasyCad"
                             ". Waarschijnlijk antwoordt EasyCad tijdelij"
                             "k te traag. Gelieve opnieuw te proberen.")
        return results
    if server.start_page == "WrongCredentials":
        results["result_class"] = "resultNOK"
        results["message"] = "Login failed!"
        results["details"] = "Controleer uw gebruikersnaam en paswoord."
        return results
    # For normal users, the startpage after login is the webchart.
    # If not the case, we have to do an extra step and browse to it:
    if not server.start_page == "WebChart":
        if not server.get_beschikbaarheid():
            results["result_class"] = "resultNOK"
            results["message"] = "Failed to load page!"
            results["details"] = ("Applicatie is ingelogd, maar kan de  "
                                 "pagina \"Beschikbaarheid\" niet laden.")
            return results
    set_user_info(username, password, server.user_real_name)
    results["result_class"] = "resultOK"
    results["message"] = server.current_availability
    results["details"] = ""
    return results

def set_user_info(username, password, real_name):
    """Stores or updates information about the user in the datastore."""
    user_info = UserInfo.get_by_key_name(username)
    if not user_info:
        user_info = UserInfo(key_name=username)
    user_info.password = password
    user_info.real_name = real_name
    user_info.put()
