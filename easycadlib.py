# coding=utf-8

"""
# beschikbaarheidslevels:
# "2" = 5 plus
# "3" = Wacht Kazerne
# "4" = AMB/PO
# "5" = Tijdelijk in kazerne
# "6" = 5 min
# "7" = 10 min
# "8" = 15 min
# "9" = Reserve
# "10" = OffvW ALO
# "11" = Niet Beschikbaar
# "12" = 10 plus
# "13" = 15 plus
# "16" = In VTO in kazerne
# "17" = Onderweg naar kazerne

"""

import urllib
import urllib2
import logging
import statetable
import datetime

BASE_URL = "https://easycad.zvbw.be"

class EasyCadServer():
    """Class that represents the website https://easycad.zvbw.be/ as an object
    with a certain state and a set of operations that can be performed on it.

    """

    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.user_real_name = "Unknown"
        self.current_personel_state = "N/A"
        self.current_availability = "N/A"
        self.eventvalidation = "not initialized"
        self.viewstate = "not initialized"
        self.cookie = ""
        if self._get_initial_page():
            self.start_page = self.login(username, password)
        else:
            self.start_page = None

    def _get_initial_page(self):
        """Tries to load just the first login page of easycad.zvbw.be."""
        try:
            url = BASE_URL
            response = urllib2.urlopen(url, timeout=8)
            cookie = response.info()["set-cookie"]
            self.cookie = cookie[0:cookie.find(';')]
            source = response.read()
            self._extract_asp_parms(source)
            return True
        except Exception:
            logging.exception("Caught general Exception!")
            return False

    def _extract_asp_parms(self, source):
        """Extracts the asp parameters "viewstate" and "eventvalidation".

        These two parameters become part of the server's internal state and are
        used as parameters in the next call to easycad.

        """
        self.viewstate = self._extract_input(source, "__VIEWSTATE")
        self.eventvalidation = self._extract_input(source, "__EVENTVALIDATION")

    def _extract_input(self, source, parameter_id):
        """Extracts the parameter with a given id from a html source and
        returns the corresponding value.

        """
        start = source.find('id="%s"' % parameter_id)
        start = source.find('value', start)
        start = source.find('"', start)
        end = source.find('"', start + 1)
        return source[start+1:end]

    def _extract_user_real_name(self, source):
        """In one of the pages we navigate through, the full name of the
        current user is visible. This is slightly different depending on the
        user's rights in the system.
        This method extracts the username from the html code of this page and
        sets it as a general variable on this server.

        """
        text_before = "id=\"lblUserName\""
        if text_before in source:
            start = source.find(text_before) + len(text_before)
            start = source.find(">", start) + 1
            text_after = "</span>"
            end = source.find(text_after, start)
            self.user_real_name = source[start:end]
            logging.debug("Real name: %s", self.user_real_name)
            return
        text_before = "id=\"cboPersonnel\""
        if text_before in source:
            start = source.find(text_before) + len(text_before)
            text_before = "selected=\"selected\""
            start = source.find(text_before, start) + len(text_before)
            start = source.find(">", start) + 1
            text_after = "</option>"
            end = source.find(text_after, start)
            self.user_real_name = source[start:end]
            logging.debug("Real name: %s", self.user_real_name)
            return
        else:
            logging.debug("Failed to get real username!")
            self.user_real_name = "Onbekend"

    def _extract_current_availability(self, source):
        """Extracts the user's current availablity state from the given html
        input and sets it as a general variable on this server.
        """
        text_before = "Huidig beschikbaarheidsniveau : "
        text_after = "</span>"
        if text_before in source:
            start = source.find(text_before) + len(text_before)
            end = source.find(text_after, start)
            self.current_availability = source[start:end]
        else:
            logging.debug("Failed to get current availability!")
            self.current_availability = "Onbekend"

    def _extract_personel_state(self, source):
        """Extracts the current availablity state of all personel. As input it
        uses the source html of the page displaying the colourful table that
        shows all the personel in a nice color, corresponding to their state.
        The result is set as a general variable on this server.

        """
        text_before = "id=\"myGridView\""
        if text_before in source:
            names_and_states = {}
            start = source.find(text_before) + len(text_before)
            text_after = "</table>"
            end = source.find(text_after, start)
            table = source[start:end]
            cell_start = 0
            cell_end = 0
            cell_start_txt = "<td"
            cell_end_txt = "</td>"
            while (cell_start > -1 and cell_start < table):
                cell_start = table.find(cell_start_txt, cell_end)
                cell_end = (table.find(cell_end_txt, cell_start) +
                            len(cell_end_txt))
                cell = table[cell_start:cell_end]
                name_and_state = statetable.cell_extract_name_and_state(cell)
                if name_and_state:
                    name, state = name_and_state
                    names_and_states[name] = state
            result = statetable.summarize_names_and_states(names_and_states)
            self.current_personel_state = result
        else:
            self.current_personel_state = "Momenteel niet beschikbaar"

    def login(self, username, password):
        """Log in to easycad and hence bring the server into a logged in state.

        If it succeeds the result will be a string representing the page we
        arrived on after the simple login. This might be different depending
        on the rights of the user.
        If it fails because the credentials are wrong, it will also tell so.
        If it fails otherwise, we return False.

        """
        url = BASE_URL
        data = {
                "__VIEWSTATE": self.viewstate,
                "__EVENTVALIDATION": self.eventvalidation,
                "ctl00$MainContent$txtUserName": username,
                "ctl00$MainContent$txtPassword": password,
                "ctl00$MainContent$buLogin": "Login",
               }
        request = urllib2.Request(url, urllib.urlencode(data))
        request.add_header("Cookie", self.cookie)
        try:
            response = urllib2.urlopen(request, timeout=8)
            result = response.read()
            if "action=\"WebChart.aspx" in result:
                logging.debug("%s arrived on WebChart after login.", username)
                self._extract_asp_parms(result)
                self._extract_current_availability(result)
                self._extract_user_real_name(result)
                return "WebChart"
            if "action=\"InterventieViewer.aspx" in result:
                logging.debug("%s arrived on InterventieViewer after login.",
                              username)
                self._extract_asp_parms(result)
                return "InterventieViewer"
            if "U heeft geen toegang tot deze applicatie." in result:
                logging.debug("%s gave wrong credentials.",
                              username)
                self._extract_asp_parms(result)
                return "WrongCredentials"
            return False
        except Exception:
            logging.exception("Caught general Exception!")
            return False

    def get_beschikbaarheid(self):
        """Tries to browse to the availability webpage.
        The resulting webpage will contain a textual representation of the
        user's current state, which we will extract.
        The resulting webpage also contains a colorful representation of the
        users status in the entire month, with which we will do nothing, since
        the information does not contain enough detail and is hence useless.

        """
        logging.debug("Trying to go to \"Beschikbaarheid\".")
        url = "%s/InterventieViewer.aspx" % BASE_URL
        data = {
                "__EVENTTARGET": "ctl00$MenuList",
                "__EVENTARGUMENT": "1",
                "__LASTFOCUS": "",
                "__VIEWSTATE": self.viewstate,
                "__VIEWSTATEENCRYPTED": "",
                "__EVENTVALIDATION": self.eventvalidation,
                "ctl00$MainContent$txtPageSize": "30",
               }
        request = urllib2.Request(url, urllib.urlencode(data))
        request.add_header("Cookie", self.cookie)
        response = urllib2.urlopen(request, timeout=15)
        result = response.read()
        if "action=\"WebChart.aspx" in result:
            self._extract_asp_parms(result)
            self._extract_current_availability(result)
            self._extract_user_real_name(result)
            return "WebChart"
        return False

    def get_beschikbaarheid_ingeven(self):
        """Tries to browse to the form where we can set our availability state.
        Before we do this, we must first navigate to the availability webpage.

        """
        url = "%s/WebChart.aspx" % BASE_URL
        data = {
                "__VIEWSTATE": self.viewstate,
                "__EVENTVALIDATION": self.eventvalidation,
                "ctl00$MainContent$btnAddAvail": "Beschikbaarheid ingeven",
                "ctl00$MainContent$ddlMonth": self._current_month(),
                "ctl00$MainContent$CheckBoxOproepbaarVolgensPlanning": "on",
               }
        request = urllib2.Request(url, urllib.urlencode(data))
        request.add_header("Cookie", self.cookie)
        response = urllib2.urlopen(request, timeout=15)
        result = response.read()
        if "Uw huidige beschikbaarheid is:" in result:
            self._extract_asp_parms(result)
            return True
        return False

    def set_beschikbaarheid(self, level, start, end):
        """Tries to submit a certain availability state and start and end time.
        Before we do this, we must first navigate to the form where we can set
        our availability state.

        """
        start_time = start.strftime("%H:%M")
        end_time = end.strftime("%H:%M")
        url = "%s/Availability.aspx" % BASE_URL
        data = {
                "__VIEWSTATE": self.viewstate,
                "__EVENTVALIDATION": self.eventvalidation,
                "ctl00$MainContent$ChangeAvailability":
                    "RadioBtnChangeAvailability",
                "ctl00$MainContent$ddlAvailability": level,
                "MainContent_C1DateTimeStart__jsonserverstate_ffcache":
                    self.date_picker_result(start),
                "ctl00$MainContent$teksten": "uur,minuten",
                "ctl00$MainContent$C1TimeStart": "%s" % start_time,
                "MainContent_C1DateTimeStop__jsonserverstate_ffcache":
                    self.date_picker_result(end),
                "ctl00$MainContent$C1TimeStop": "%s" % end_time,
                "ctl00$MainContent$buConfirm": "Bevestigen",
               }
        request = urllib2.Request(url, urllib.urlencode(data))
        request.add_header("Cookie", self.cookie)
        response = urllib2.urlopen(request, timeout=15)
        result = response.read()
        if "Huidig beschikbaarheidsniveau" in result:
            self._extract_asp_parms(result)
            return True
        return False

    def date_picker_result(self, datetime):
        """Formats a given datetime into a horrible complex string representing
        what the fancy datetime picker used by the easycad system would return.

        """
        date_format1 = datetime.strftime("%Y-%m-%d")
        date_format2 = datetime.strftime("%d/%m/%Y")
        result = ('{"disabled":false,"culture":"nl-BE","invalidClass":"ui-st'
                  'ate-error","nullText":"","showNullText":false,"hideEnter"'
                  ':false,"disableUserInput":false,"buttonAlign":"right","sh'
                  'owTrigger":true,"showSpinner":false,"initializing":null,"'
                  'initialized":null,"triggerMouseDown":null,"triggerMouseUp'
                  '":null,"textChanged":null,"invalidInput":null,"date":"%sT'
                  '00:00:00:000Z","minDate":null,"maxDate":null,"dateFormat"'
                  ':"dd/MM/yyyy","startYear":1950,"smartInputMode":true,"act'
                  'iveField":2,"keyDelay":800,"autoNextField":true,"calendar'
                  '":"default","popupPosition":{"offset":"0 4"},"dateChanged'
                  '":null,"postBackEventReference":null,"autoPostBack":false'
                  ',"comboItems":[],"disabledState":false,"text":"%s"}')
        return result % (date_format1, date_format2)

    def get_personeelstatus(self, station):
        """Tries to browse to the personel availability overview webpage.
        The resulting webpage contains a colorful table representing all the
        persons for the default station. This table will be parsed.
        If it's not the correct station, we have to change the tab.

        """
        logging.debug("Trying to go to \"Personeelstatus\".")
        url = "%s/%s.aspx" % (BASE_URL, self.start_page)
        if self.start_page == "WebChart":
            data = {
                    "__EVENTTARGET": "ctl00$MenuList",
                    "__EVENTARGUMENT": "6",
                    "__LASTFOCUS": "",
                    "__VIEWSTATE": self.viewstate,
                    "__EVENTVALIDATION": self.eventvalidation,
                    "ctl00$MainContent$CheckBoxOproepbaarVolgensPlanning": "on",
                    "ctl00$MainContent$ddlMonth": self._current_month(),
                   }
        elif self.start_page == "InterventieViewer":
            data = {
                    "__EVENTTARGET": "ctl00$MenuList",
                    "__EVENTARGUMENT": "6",
                    "__LASTFOCUS": "",
                    "__VIEWSTATE": self.viewstate,
                    "__VIEWSTATEENCRYPTED": "",
                    "__EVENTVALIDATION": self.eventvalidation,
                    "ctl00$MenuTable$ctl03$ctl02": "Personeelstatus",
                    "ctl00$MainContent$txtPageSize": "10",
                   }
        request = urllib2.Request(url, urllib.urlencode(data))
        request.add_header("Cookie", self.cookie)
        response = urllib2.urlopen(request, timeout=15)
        result = response.read()
        station_rights = self._parse_station_rights(result)
        if "action=\"PersoneelsViewer.aspx" in result:
            self._extract_asp_parms(result)
            # automatically the first page on which you have rights is loaded
            if station_rights[0] == station:
                self._extract_personel_state(result)
                return "PersoneelsViewer"
            # if the first page is not the requested one
            # we check if we have access to it and browse to it
            elif station in station_rights:
                result = self._get_personeelstatus_tab(station)
                # For future reference: To send the output to the server
                # you can put server.literaloutput in the html output.
                ## self.literaloutput = result
                if "action=\"./PersoneelsViewer.aspx" in result:
                    self._extract_asp_parms(result)
                    self._extract_personel_state(result)
                    return "PersoneelsViewer"
        return False

    def _get_personeelstatus_tab(self, station):
        """Tries to change the station tab on the personel availability
        overview webpage and returns the resulting html.

        """
        # Default:
        btn = "ctl00$MainContent$btnTabBlad0"
        station_labels = [
                          "ASS", "ASS+",
                          "DIL", "DIL+",
                          "HAL", "HAL+",
                          "LEN", "LEN+",
                          "LON", "LON+",
                          "OPW", "OPW+",
                          "TOL", "TOL+",
                          "VIL", "VIL+",
                          "ZAV", "ZAV+",
                         ]
        for i in range(len(station_labels)):
          if station == station_labels[i]:
              btn = "ctl00$MainContent$btnTabBlad%d" % i
        logging.debug("Trying to browse to tab \"%s\".", station)
        url = "%s/PersoneelsViewer.aspx" % BASE_URL
        data = {"__EVENTTARGET": "",
                "__EVENTARGUMENT": "",
                "__VIEWSTATE": self.viewstate,
                "__EVENTVALIDATION": self.eventvalidation,
                "ctl00$MainContent$selectedAvailability": "",
                btn: "DIL",}

        request = urllib2.Request(url, urllib.urlencode(data))
        request.add_header("Cookie", self.cookie)
        response = urllib2.urlopen(request, timeout=15)
        return response.read()

    def _parse_station_rights(self, source):
        """Parses the given html page (which should be the personel
        availability overview webpage) and extracts a list of all the fire
        stations to which the user has the right to browse to.

        """
        rights = []
        start = 0
        for i in range(7):
            text_before = "name=\"ctl00$MainContent$btnTabBlad%d\" value=\"" % i
            text_between = "\" id=\"MainContent_btnTabBlad%d\" " % i
            if text_before in source:
                start = source.find(text_before, start) + len(text_before)
                end = source.find(text_between, start)
                station = source[start:end]
                disabled = "disabled=\"disabled\""
                start = end + len(text_between)
                end = start + len(disabled)
                if not source[start:end] == disabled:
                    rights.append(station)
        return rights

    def user_info(self):
        """Returns a nicely formatted string representation of the current user.

        """
        return "%s - %s - %s" % (self.user_real_name,
                                 self.username,
                                 self.password)

    def _current_month(self):
        """Returns a string representing the current month, in the strange
        format in which asp.net wants it. E.g. "2015-1"

        """
        now = datetime.datetime.now()
        return "{}-{}".format(now.year, now.month)
