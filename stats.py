""""Contains all that is necessary to show the "stats" webpage.

Consists of
- The class Stats: The webpage itself.

"""

import webapp2
from domain import StationState, StationHistory
from datetime import datetime

class Stats(webapp2.RequestHandler):
    """Very basic webpage that generates a listing of stationstate snapshots.
    Intended to be used for later processing.

    """
    def get(self):
        """Http get method/operation on the stats page address.

        Returns a very simple stats page, not even as html, but just as simple
        text.

        """
        # This page is usually in a disabled state to avoid people from
        # getting access to all the statistics information.

        # This outputs a serialized version of the StationHistories
        # We have separate scripts to parse this
        ##lines = []
        ##year = 2017
        ##start = datetime(year, 1, 1)
        ##end = datetime(year, 12, 31)
        ### get StationStates -> later changed to StationHistories
        ##for station_history in StationHistory.all()\
        ##                                     .order("day_date")\
        ##                                     .filter("day_date >=", start)\
        ##                                     .filter("day_date <=", end):
        ##    ##lines.append("%s" % str(station_history))
        ##    lines.append("%s" % station_history.serialize())
        ##stat_lines = "<br>\n".join(lines)
        ##self.response.out.write(stat_lines)

        # Old migration code used when migrating from StationState to
        # StationHistory just kept here for later reference (you never know...)
        ##station_history = None
        ##for station_state in StationState.all().order("timeblock"):
        ##    day = station_state.timeblock.date()
        ##    if day.month == 12 and day.day == 21:
        ##        tb = station_state.timeblock
        ##        index = (tb.hour * 60 + tb.minute) / 15
        ##        if not station_history or not station_history.day_date == day:
        ##            station_history = StationHistory.all().filter(
        ##                                             "day_date =", day).get()
        ##        if not station_history:
        ##                # If it doesn't exist, we create it
        ##                station_history = StationHistory()
        ##                station_history.initialize(station_state.station, day)
        ##         # Now add the current station state to today's record
        ##        station_history.set_station_state(index, station_state)
        ##        # And store it in the db (only if we filled the last slot)
        ##        if index == 48 or index == 49 or index == 50:
        ##            station_history.put()
        ##self.response.out.write("Tes hedoan")

        self.response.write("This is the stats page")

