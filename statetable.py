"""Contains methods to help with the parsing of the personel state table."""
# coding=utf-8

from domain import StationState

def cell_extract_name_and_state(cell):
    """Takes the html representing one cell of the table as input and will
    extract the availability state (based on the cell's background color) and
    the name of the person (based on the text displayed in the cell).
    The availability state is returned as an integer, with -1 meaning "on
    intervention", 0 meaning "in the station", 2 meaning "in station exercise"
    and other numbers just meaning in how many minutes the person can be in
    the station.
    The 5+, 10+ and 15+ states are represented by 6, 11 and 16.

    """
    text_before = "bgcolor=\""
    start = cell.find(text_before) + len(text_before)
    end = cell.find("\"", start)
    color = cell[start:end]
    text_before = ">"
    start = cell.find(text_before, end + 15) + len(text_before)
    end = cell.find("<", start)
    name = cell[start:end]
    if color == "#FFFF80" or color == "Yellow":
        return (name, -1)
    # #00FF40 = Wacht Kazerne
    # #80FF80 = Onderweg naar Kazerne
    # #FF80FF = AMB
    # #00FF00/Lime = Wacht Kazerne
    # #8000FF = Tijdelijk in kazerne
    if (color == "#00FF40" or color == "#80FF80" or
        color == "#FF80FF" or color == "Lime" or color == "#8000FF"):
        return (name, 0)
    # #FF8080 = In VTO in kazerne --> we call this "2 minutes"
    if color == "#FF8080":
        return (name, 2)
    if color == "#8080FF" or color == "#0080FF": # 5 min
        return (name, 5)
    if color == "#FF8040" or color == "#FF8000": # 10 min
        return (name, 10)
    if color == "#B36666": # 15 min
        return (name, 15)
    if color == "#0066CC": # 5 plus
        return (name, 6)
    if color == "#C46200": # 10 plus
        return (name, 11)
    if color == "#804040": # 15 plus
        return (name, 16)
    if color == "Aqua":
        return (name, 30)

def summarize_names_and_states(names_and_states):
    """Takes a dictionary of all knowns names (=key) and their corresponding
    availability states (=value) and returns an instance of the StationState
    class, which has a str method that returns a nice multi-line html string
    giving an overview of all the availability states and the persons who are
    in that state.
    Expected values for the state are the ones returned by the
    cell_extract_name_and_state method.

    """
    firechiefs = 0
    firefighters = 0
    list_per_state = {-1:[], 0:[], 2:[], 5:[], 10:[], 15:[], 6:[], 11:[], 16:[], 30:[],}
    for name, state in names_and_states.iteritems():
        if state < 30:
            if _is_firechief(name):
                firechiefs += 1
            else:
                firefighters += 1
        list_per_state[state].append(name)

    station_state = StationState()
    station_state.firechiefs_count = firechiefs
    station_state.firefighters_count = firefighters
    station_state.on_intervention = list_per_state[-1]
    station_state.in_station = list_per_state[0]
    station_state.in_VTO_in_station = list_per_state[2]
    station_state.on_5_minutes = list_per_state[5]
    station_state.on_10_minutes = list_per_state[10]
    station_state.on_15_minutes = list_per_state[15]
    station_state.on_5_plus_minutes = list_per_state[6]
    station_state.on_10_plus_minutes = list_per_state[11]
    station_state.on_15_plus_minutes = list_per_state[16]
    station_state.in_reserve = list_per_state[30]
    return station_state

def _is_firechief(name):
    """Returns for a given name if the person is a chief ("gradé").

    The list of chiefs is hardcoded and the will only work for people in the
    Dilbeek fire station (by explicit design).
    All
      - people with a rank of Sergeant or higher or
      - people with rank of Corporal and trained as Sergeant
    are considered chiefs that can be in charge of an engine.

    """
    name_upper = name.upper()
    if name_upper in ["WIJNS DANNY", "DELOMBAERDE LUC", "DEDOBBELEER KURT",
                      "BROEKAERT WERNER", "SADONES JAN",
                      "VAN DEN HOUTE JURGEN",
                      "HABSCH SVEN",]:
        return True
