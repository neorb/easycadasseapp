"""Methods used to render the graph page"""

from os import path
import jinja2
TEMPLATES_DIR = path.join(path.dirname(__file__), "templates")
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(TEMPLATES_DIR))

import StringIO
import logging
from datetime import datetime, timedelta
import matplotlib as mpl
##from matplotlib import patheffects
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg
import matplotlib.dates as mdates
from matplotlib.ticker import MultipleLocator
from domain import UserActionCount, StationState

def show():
    """Generate html for the graph page."""
    _set_matplotlib_parameters()

    dates, actions = _collect_usage_data()
    fig = _create_usage_graph_figure(dates, actions)
    usage_image_src = _generate_image_src(fig)

    times, state_data = _collect_station_state_data()
    fig = _create_station_state_graph_figure(times, state_data)
    station_state_image_src = _generate_image_src(fig)

    template = JINJA_ENVIRONMENT.get_template("graph.html")
    return template.render({
                    "usage_image_src": usage_image_src,
                    "station_state_image_src": station_state_image_src,
                            })

# Region Usage data

def _collect_usage_data():
    """Gets the data about the site usage in the last 50 days from the
    datastore and formats it in such a way that it can be used by matplotlib.

    """
    dates = []
    actions = []
    for cnt in UserActionCount.all().order('-__key__').fetch(50):
        dates.append(datetime.strptime(cnt.key().name(), "%Y%m%d").date())
        actions.append(cnt.count)
    dates.reverse()
    actions.reverse()
    return dates, actions

def _create_usage_graph_figure(dates, actions):
    """Uses matplotlib to create a bar plot given even a set of dates and the
    set of corresponding amounts of user actions on those days.

    """
    weekdayts = []
    weekdayac = []
    weekendts = []
    weekendac = []
    for i in range(len(dates)):
        if dates[i].weekday() in range(5):
            weekdayts.append(dates[i])
            weekdayac.append(actions[i])
        else:
            weekendts.append(dates[i])
            weekendac.append(actions[i])

    fig = Figure(dpi=100, figsize=(10, 6))
    axes = fig.add_axes([0.1, 0.2, 0.85, 0.75])
    axes.set_ylabel('Gebruik per dag')
    axes.set_xlabel('Tijd')

    axes.bar(weekdayts, weekdayac,
           align='center', width=1,
           facecolor='yellow', edgecolor='r')
    axes.bar(weekendts, weekendac,
           align='center', width=1,
           facecolor='yellow', edgecolor='b')

    begin = dates[0] + timedelta(hours=6)
    end = dates[-1] + timedelta(days=3)
    axes.axis([begin, end, 0, max(actions)+5])
    fig.autofmt_xdate()
    axes.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y'))

    return fig

# Region Helper methods

def _set_matplotlib_parameters():
    """Sets some parameters for matplotlib."""
    mpl.rcParams['lines.linewidth'] = 6
    mpl.rcParams['font.family'] = ['Humor Sans', 'Comic Sans MS']
    mpl.rcParams['font.size'] = 14.0
    # Following 2 params commented out because they simply don't exist yet
    # in version 1.2.0 of Matplotlib (used by GAE)
    # We'll have to wait for GAE to support version 1.3.0
    ##mpl.rcParams['path.sketch'] = (1, 100, 2)
    ##mpl.rcParams['path.effects'] = [patheffects.withStroke(
    ##                                      linewidth=4, foreground="w")]
    mpl.rcParams['axes.linewidth'] = 1.5
    mpl.rcParams['lines.linewidth'] = 2.0
    mpl.rcParams['figure.facecolor'] = 'white'
    mpl.rcParams['grid.linewidth'] = 0.0
    mpl.rcParams['axes.unicode_minus'] = False
    mpl.rcParams['axes.color_cycle'] = ['b', 'r', 'c', 'm']
    mpl.rcParams['xtick.major.size'] = 8
    mpl.rcParams['xtick.major.width'] = 3
    mpl.rcParams['ytick.major.size'] = 8
    mpl.rcParams['ytick.major.width'] = 3

def _generate_image_src(figure):
    """Takes a Matplotlig Figure and returns the corresponding encoded png
    source to be used in a webpage.

    """
    FigureCanvasAgg(figure)
    image_str = StringIO.StringIO()
    figure.savefig(image_str)
    image_src = image_str.getvalue().encode("base64").strip()
    return image_src
